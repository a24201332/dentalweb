from django.urls import path
from . import views

urlpatterns=[
path('', views.inicio, name='inicio'),

#33333333333333333333333333333333
path('paciente/',views.listadoPaciente),
path('guardarPaciente/',views.guardarPaciente),
path('listaPacientes/', views.listaPacientes, name='listaPacientes'),
path('eliminarPaciente/<id>',views.eliminarPaciente),
path('editarPaciente/<id>',views.editarPaciente),
path('procesarActualizacionPaciente/<id>', views.procesarActualizacionPaciente, name='procesarActualizacionPaciente'),


path('cita/',views.listadoCita),
path('guardarCita/',views.guardarCita),
path('listadoCitas/', views.listadoCitas, name='listadoCitas'),
path('eliminarCita/<id>',views.eliminarCita),
path('editarCita/<id>',views.editarCita),
path('procesarActualizacionCita/<id>', views.procesarActualizacionCita, name='procesarActualizacionCita'),


path('control/',views.listadoControl),
path('guardarControl/',views.guardarControl),
path('listadoControles/', views.listadoControles, name='listadoControles'),
path('eliminarControl/<id>',views.eliminarControl),
path('editarControl/<id>',views.editarControl),
path('procesarActualizacionControl/<id>', views.procesarActualizacionControl, name='procesarActualizacionControl'),


path('receta/',views.listadoReceta),
path('guardarReceta/',views.guardarReceta),
path('listadoRecetas/', views.listadoRecetas, name='listadoRecetas'),
path('eliminarReceta/<id>',views.eliminarReceta),
path('editarReceta/<id>',views.editarReceta),
path('procesarActualizacionReceta/<id>', views.procesarActualizacionReceta, name='procesarActualizacionReceta'),


path('tratamiento/',views.listadoTratamiento),
path('guardarTratamiento/',views.guardarTratamiento),
path('listadoTratamientos/', views.listadoTratamientos, name='listadoTratamientos'),
path('eliminarTratamiento/<id>',views.eliminarTratamiento),
path('editarTratamiento/<id>',views.editarTratamiento),
path('procesarActualizacionTratamiento/<id>', views.procesarActualizacionTratamiento, name='procesarActualizacionTratamiento'),


path('pago/',views.listadoPago),
path('guardarPago/',views.guardarPago),
path('listadoPagos/', views.listadoPagos, name='listadoPagos'),
path('eliminarPago/<id>',views.eliminarPago),
path('editarPago/<id>',views.editarPago),
path('procesarActualizacionPago/<id>', views.procesarActualizacionPago, name='procesarActualizacionPago'),


path('clinico/',views.listadoClinico),
path('guardarClinico/',views.guardarClinico),
path('listadoClinicos/', views.listadoClinicos, name='listadoClinicos'),
path('eliminarClinico/<id>',views.eliminarClinico),
path('editarClinico/<id>', views.editarClinico, name='editarClinico'),
path('procesarActualizacionClinico/<id>', views.procesarActualizacionClinico, name='procesarActualizacionClinico'),
]
