from django.db import models

# Create your models here.
class Paciente(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=150)
    cedula = models.CharField(max_length=150)
    edad = models.IntegerField()
    genero = models.CharField(max_length=150)
    direccion = models.CharField(max_length=150)
    email = models.CharField(max_length=150)
    foto = models.FileField(upload_to='fotos', null=True, blank=True)
    estado = models.CharField(max_length=150)



    def __str__(self):
        fila = "{0}: {1} - {2} a {3}"
        return fila.format(self.id, self.nombre, self.cedula, self.edad)

class Cita(models.Model):
    id = models.AutoField(primary_key=True)
    estado = models.CharField(max_length=150)
    fecha = models.DateTimeField()
    paciente = models.ForeignKey(Paciente, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0}: {1} {2} - {3}"
        return fila.format(self.id, self.estado, self.fecha, self.paciente)

        

class Control(models.Model):
    id = models.AutoField(primary_key=True)
    estado = models.CharField(max_length=150)
    fecha = models.DateTimeField()
    descripcion=models.TextField()
    paciente = models.ForeignKey(Paciente, null=True, blank=True, on_delete=models.PROTECT)



    def __str__(self):
        fila = "{0}: {1} - {2} a {3}"
        return fila.format(self.id, self.estado, self.fecha, self.descripcion)


class Receta(models.Model):
    id = models.AutoField(primary_key=True)
    fechaPrescripcion = models.DateField()
    medicamento = models.CharField(max_length=150)
    duracion = models.CharField(max_length=150)
    receta=models.TextField()
    instrucciones = models.CharField(max_length=300)
    paciente = models.ForeignKey(Paciente, null=True, blank=True, on_delete=models.PROTECT)



    def __str__(self):
        fila = "{0}: {1} - {2} a {3}"
        return fila.format(self.id, self.fechaPrescripcion, self.medicamento, self.duracion)

class Tratamiento(models.Model):
    id = models.AutoField(primary_key=True)
    tipoTratamiento = models.CharField(max_length=150)
    detalles=models.TextField()
    fechaInicio = models.DateField()
    fechaFin = models.DateField()
    estado = models.CharField(max_length=150)
    instrucciones = models.CharField(max_length=300)
    paciente = models.ForeignKey(Paciente, null=True, blank=True, on_delete=models.PROTECT)



    def __str__(self):
        fila = "{0}: {1} - {2} a {3}"
        return fila.format(self.id, self.tipoTratamiento, self.fechaInicio, self.fechaFin)

class Pago(models.Model):
    id = models.AutoField(primary_key=True)
    fechaHora = models.DateTimeField()
    totalPago = models.DecimalField(max_digits=10, decimal_places=2)
    montoAbonado = models.DecimalField(max_digits=10, decimal_places=2)
    debe = models.DecimalField(max_digits=10, decimal_places=2)
    estado = models.CharField(max_length=150)
    paciente = models.ForeignKey(Paciente, null=True, blank=True, on_delete=models.PROTECT)



    def __str__(self):
        fila = "{0}: {1} - {2} a {3}"
        return fila.format(self.id, self.fechaHora, self.totalPago, self.montoAbonado)

class Clinico(models.Model):
    id = models.AutoField(primary_key=True)
    paciente = models.ForeignKey(Paciente, related_name='clinico_paciente', null=True, blank=True, on_delete=models.PROTECT)
    edad = models.ForeignKey(Paciente, related_name='clinico_edad', null=True, blank=True, on_delete=models.PROTECT)
    cedula = models.ForeignKey(Paciente, related_name='clinico_cedula', null=True, blank=True, on_delete=models.PROTECT)
    genero = models.ForeignKey(Paciente, related_name='clinico_genero', null=True, blank=True, on_delete=models.PROTECT)
    fecha = models.ForeignKey(Cita, related_name='clinico_fecha', null=True, blank=True, on_delete=models.PROTECT)
    genero = models.ForeignKey(Paciente, related_name='clinico_genero', null=True, blank=True, on_delete=models.PROTECT)
    descripcion = models.ForeignKey(Control, related_name='clinico_descripcion', null=True, blank=True, on_delete=models.PROTECT)
    foto = models.ForeignKey(Paciente, related_name='clinico_foto', null=True, blank=True, on_delete=models.PROTECT)
    tipoTratamiento = models.ForeignKey(Tratamiento, related_name='clinico_tratamiento', null=True, blank=True, on_delete=models.PROTECT)
    estado = models.ForeignKey(Paciente, related_name='clinico_estado', null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0}: {1} - {2} a {3}"
        return fila.format(self.id, self.paciente, self.edad, self.cedula)
