# Generated by Django 4.2.7 on 2024-02-08 08:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('DENTALWEB', '0002_cita'),
    ]

    operations = [
        migrations.CreateModel(
            name='Control',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('estado', models.CharField(max_length=150)),
                ('fecha', models.DateTimeField()),
                ('descripcion', models.TextField()),
                ('paciente', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='DENTALWEB.paciente')),
            ],
        ),
    ]
