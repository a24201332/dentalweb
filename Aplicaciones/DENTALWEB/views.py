from django.shortcuts import render, redirect
from datetime import datetime
from .models import Paciente
from .models import Cita
from .models import Control
from .models import Tratamiento
from .models import Receta
from .models import Pago
from .models import Clinico
from django.contrib import messages
from django.conf import settings
from django.http import HttpResponseRedirect
from django.core.mail import send_mail



# Create your views here.


def inicio(request):
    return render(request, 'inicio.html')

def listadoPaciente(request):
    pacientesBdd =Paciente.objects.all()
    return render(request, 'paciente.html',{'pacientes':pacientesBdd})

def listaPacientes(request):
    pacientes = Paciente.objects.all()
    return render(request, 'listaPacientes.html', {'pacientes': pacientes})


def guardarPaciente(request):
    nombre = request.POST["nombre"]
    cedula = request.POST["cedula"]
    edad = request.POST["edad"]
    genero = request.POST["genero"]
    direccion = request.POST["direccion"]
    email = request.POST["email"]
    foto=request.FILES.get("foto")
    estado = request.POST["estado"]

    # Formatear la fecha correctamente


    # Insertando datos mediante ORM de Django
    nuevoPaciente = Paciente.objects.create(
        nombre=nombre,
        cedula=cedula,
        edad=edad,
        genero=genero,
        direccion=direccion,
        email=email,
        foto=foto,
        estado=estado
    )
    send_mail(
        'Asunto del correo electrónico',
        'Hola ' + nombre + ', tu registro como paciente ha sido exitoso.',
        'javiertuso30@gmail.com',
        [email],
        fail_silently=False,
    )

    messages.success(request, 'Creado Paciente exitosamente ')
    return redirect('/listaPacientes/')


def eliminarPaciente(request, id):
    pacienteEliminar = Paciente.objects.get(id=id)
    pacienteEliminar.delete()
    messages.error(request, 'Eliminado el paciente exitosamente ')
    return redirect('/listaPacientes/')


def editarPaciente(request, id):
    pacienteEditar= Paciente.objects.get(id=id)
    return render(request, 'editarPaciente.html', {'paciente': pacienteEditar})


def procesarActualizacionPaciente(request, id):
    id = request.POST["id"]
    nombre = request.POST["nombre"]
    cedula = request.POST["cedula"]
    edad = request.POST["edad"]
    genero = request.POST["genero"]
    direccion = request.POST["direccion"]
    email = request.POST["email"]
    foto=request.FILES.get("foto")
    estado = request.POST["estado"]


    # Insertando datos mediante ORM de Django
    pacienteEditar = Paciente.objects.get(id=id)
    pacienteEditar.nombre = nombre
    pacienteEditar.cedula = cedula
    pacienteEditar.edad = edad
    pacienteEditar.genero = genero
    pacienteEditar.direccion = direccion
    pacienteEditar.email = email
    pacienteEditar.foto = foto
    pacienteEditar.estado = estado
    pacienteEditar.save()

    messages.success(request, 'Editado el paciente exitosamente ')
    return redirect('/listaPacientes/')
#33333333333333333333333333333333

def listadoCita(request):
    citasBdd =Cita.objects.all()
    pacientesBdd=Paciente.objects.all()
    return render(request, 'cita.html',{'citas':citasBdd, 'pacientes':pacientesBdd})

def listadoCitas(request):
    citasBdd =Cita.objects.all()
    pacientesBdd = Paciente.objects.all()
    return render(request, 'listadoCitas.html', {'citas':citasBdd, 'pacientes':pacientesBdd})

def guardarCita(request):
    id_paciente=request.POST["id_paciente"]
#Capturandi el tipo seleccionado por el usuario
    pacienteSelecionado=Paciente.objects.get(id=id_paciente)
    estado = request.POST["estado"]
    fecha=request.POST["fecha"]


    # Insertando datos mediante ORM de Django
    nuevoCita = Cita.objects.create(
        estado=estado,
        fecha=fecha,
        paciente=pacienteSelecionado
    )

    messages.success(request, 'Guardado Cita exitosamente ')
    return redirect('/listadoCitas/')


def eliminarCita(request, id):
    citaEliminar = Cita.objects.get(id=id)
    citaEliminar.delete()
    messages.error(request, 'Eliminado la cita exitosamente ')
    return redirect('/listadoCitas/')


def editarCita(request, id):
    citaEditar= Cita.objects.get(id=id)
    pacientesBdd = Paciente.objects.all()
    return render(request, 'editarCita.html', {'cita': citaEditar,'pacientes':pacientesBdd})


def procesarActualizacionCita(request, id):

    #agregamos estas lineas
    id_paciente=request.POST["paciente"]
    pacienteSeleccionado=Paciente.objects.get(id=id_paciente)
    #estas
    estado = request.POST["estado"]
    fecha = request.POST["fecha"]


    # Insertando datos mediante ORM de Django
    citaEditar = Cita.objects.get(id=id)
    citaEditar.paciente=pacienteSeleccionado
    citaEditar.estado = estado
    citaEditar.fecha = fecha
    citaEditar.save()

    messages.success(request, 'Editado la cita exitosamente ')
    return redirect('/listadoCitas/')

#333333333333333333333333333333333333333333

def listadoControl(request):
    controlesBdd = Control.objects.all()
    pacientesBdd = Paciente.objects.all()
    return render(request, 'control.html', {'controles': controlesBdd, 'pacientes': pacientesBdd})

def listadoControles(request):
    controles = Control.objects.all()
    pacientes = Paciente.objects.all()
    return render(request, 'listadoControles.html', {'controles': controles, 'pacientes': pacientes})

def guardarControl(request):
    if request.method == 'POST':
        id_paciente = request.POST["id_paciente"]
        estado = request.POST["estado"]
        fecha = request.POST["fecha"]
        descripcion = request.POST["descripcion"]
        pacienteSeleccionado = Paciente.objects.get(id=id_paciente)

        nuevoControl = Control.objects.create(
            estado=estado,
            fecha=fecha,
            descripcion=descripcion,
            paciente=pacienteSeleccionado
        )

        messages.success(request, 'Guardado Control exitosamente')
        return redirect('/listadoControles/')
    else:
        messages.error(request, 'El método de solicitud no es POST')
        return redirect('/listadoControles/')

def eliminarControl(request, id):
    controlEliminar = Control.objects.get(id=id)
    controlEliminar.delete()
    messages.success(request, 'Eliminado el control exitosamente')
    return redirect('/listadoControles/')

def editarControl(request, id):
    controlEditar = Control.objects.get(id=id)
    pacientesBdd = Paciente.objects.all()
    return render(request, 'editarControl.html', {'control': controlEditar, 'pacientes': pacientesBdd})

def procesarActualizacionControl(request, id):
    if request.method == 'POST':
        dientesSeleccionados = request.POST.getlist('dientes[]')
        id_paciente = request.POST["id_paciente"]
        pacienteSeleccionado = Paciente.objects.get(id=id_paciente)
        estado = request.POST["estado"]
        fecha = request.POST["fecha"]
        descripcion = request.POST["descripcion"]

        controlEditar = Control.objects.get(id=id)
        controlEditar.estado = estado
        controlEditar.fecha = fecha
        controlEditar.descripcion = descripcion
        controlEditar.paciente = pacienteSeleccionado
        controlEditar.save()

        messages.success(request, 'Editado el control exitosamente')
        return redirect('/listadoControles/')
    else:
        messages.error(request, 'El método de solicitud no es POST')
        return redirect('/listadoControles/')
#33333333333333333333333333333333333333333333333333333333333


def listadoReceta(request):
    recetasBdd =Receta.objects.all()
    pacientesBdd=Paciente.objects.all()
    return render(request, 'receta.html',{'recetas':recetasBdd, 'pacientes':pacientesBdd})

def listadoRecetas(request):
    recetasBdd =Receta.objects.all()
    pacientesBdd = Paciente.objects.all()
    return render(request, 'listadoRecetas.html', {'recetas':recetasBdd, 'pacientes':pacientesBdd})

def guardarReceta(request):
    id_paciente=request.POST["id_paciente"]
    pacienteSelecionado=Paciente.objects.get(id=id_paciente)
    fechaPrescripcion = request.POST["fechaPrescripcion"]
    medicamento=request.POST["medicamento"]
    duracion=request.POST["duracion"]
    receta=request.POST["receta"]
    instrucciones=request.POST["instrucciones"]



    # Insertando datos mediante ORM de Django
    nuevoReceta =Receta.objects.create(
        fechaPrescripcion=fechaPrescripcion,
        medicamento=medicamento,
        duracion=duracion,
        receta=receta,
        instrucciones=instrucciones,
        paciente=pacienteSelecionado
    )

    messages.success(request, 'Guardado receta exitosamente ')
    return redirect('/listadoRecetas/')


def eliminarReceta(request, id):
    recetaEliminar = Cita.objects.get(id=id)
    recetaEliminar.delete()
    messages.error(request, 'Eliminado la receta exitosamente ')
    return redirect('/listadoRecetas/')


def editarReceta(request, id):
    recetaEditar= Receta.objects.get(id=id)
    pacientesBdd = Paciente.objects.all()
    return render(request, 'editarReceta.html', {'receta': recetaEditar,'pacientes':pacientesBdd})


def procesarActualizacionReceta(request, id):
    #agregamos estas lineas
    id_paciente=request.POST["paciente"]
    pacienteSeleccionado=Paciente.objects.get(id=id_paciente)
    #estas
    fechaPrescripcion = request.POST["fechaPrescripcion"]
    medicamento=request.POST["medicamento"]
    duracion=request.POST["duracion"]
    receta=request.POST["receta"]
    instrucciones=request.POST["instrucciones"]


    # Formatear la fecha correctamente
    try:
        fechaPrescripcion = datetime.strptime(fechaPrescripcion, "%Y-%m-%d").date()
    except ValueError:
        messages.error(request, 'Fecha de prescripcion inválida. Formato esperado: YYYY-MM-DD')
        return redirect('/listadoRecetas/')



    # Insertando datos mediante ORM de Django
    recetaEditar = Receta.objects.get(id=id)
    recetaEditar.paciente=pacienteSeleccionado
    recetaEditar.fechaPrescripcion = fechaPrescripcion
    recetaEditar.medicamento = medicamento
    recetaEditar.duracion = duracion
    recetaEditar.receta = receta
    recetaEditar.instrucciones = instrucciones
    recetaEditar.save()

    messages.success(request, 'Editado la receta exitosamente ')
    return redirect('/listadoRecetas/')

#33333333333333333333333333333333333333333333333333333333333


def listadoTratamiento(request):
    tratamientosBdd =Tratamiento.objects.all()
    pacientesBdd=Paciente.objects.all()
    return render(request, 'tratamiento.html',{'tratamientos':tratamientosBdd, 'pacientes':pacientesBdd})

def listadoTratamientos(request):
    tratamientosBdd =Tratamiento.objects.all()
    pacientesBdd = Paciente.objects.all()
    return render(request, 'listadoTratamientos.html', {'tratamientos':tratamientosBdd, 'pacientes':pacientesBdd})

def guardarTratamiento(request):
    id_paciente=request.POST["id_paciente"]
    pacienteSelecionado=Paciente.objects.get(id=id_paciente)
    tipoTratamiento = request.POST["tipoTratamiento"]
    detalles=request.POST["detalles"]
    fechaInicio=request.POST["fechaInicio"]
    fechaFin=request.POST["fechaFin"]
    estado=request.POST["estado"]
    instrucciones=request.POST["instrucciones"]



    # Formatear la fecha correctamente
    try:
        fechaInicio = datetime.strptime(fechaInicio, "%Y-%m-%d").date()
    except ValueError:
        messages.error(request, 'Fecha de nacimiento inválida. Formato esperado: YYYY-MM-DD')
        return redirect('/listadoTratamientos/')
    try:
        fechaFin = datetime.strptime(fechaFin, "%Y-%m-%d").date()
    except ValueError:
        messages.error(request, 'Fecha de nacimiento inválida. Formato esperado: YYYY-MM-DD')
        return redirect('/listadoTratamientos/')

    # Insertando datos mediante ORM de Django
    nuevoTratamiento =Tratamiento.objects.create(
        tipoTratamiento=tipoTratamiento,
        detalles=detalles,
        fechaInicio=fechaInicio,
        fechaFin=fechaFin,
        estado=estado,
        instrucciones=instrucciones,
        paciente=pacienteSelecionado
    )

    messages.success(request, 'Guardado tratamientos exitosamente ')
    return redirect('/listadoTratamientos/')


def eliminarTratamiento(request, id):
    tratamientoEliminar = Tratamiento.objects.get(id=id)
    recetaEliminar.delete()
    messages.error(request, 'Eliminado el tratamiento exitosamente ')
    return redirect('/listadoTratamientos/')


def editarTratamiento(request, id):
    tratamientoEditar= Tratamiento.objects.get(id=id)
    pacientesBdd = Paciente.objects.all()
    return render(request, 'editarTratamiento.html', {'tratamiento': tratamientoEditar,'pacientes':pacientesBdd})


def procesarActualizacionTratamiento(request, id):
    #agregamos estas lineas
    id_paciente=request.POST["paciente"]
    pacienteSeleccionado=Paciente.objects.get(id=id_paciente)
    #estas
    tipoTratamiento = request.POST["tipoTratamiento"]
    detalles=request.POST["detalles"]
    fechaInicio=request.POST["fechaInicio"]
    fechaFin=request.POST["fechaFin"]
    estado=request.POST["estado"]
    instrucciones=request.POST["instrucciones"]



    # Formatear la fecha correctamente
    try:
        fechaInicio = datetime.strptime(fechaInicio, "%Y-%m-%d").date()
    except ValueError:
        messages.error(request, 'Fecha de nacimiento inválida. Formato esperado: YYYY-MM-DD')
        return redirect('/listadoTratamientos/')
    try:
        fechaFin = datetime.strptime(fechaFin, "%Y-%m-%d").date()
    except ValueError:
        messages.error(request, 'Fecha de nacimiento inválida. Formato esperado: YYYY-MM-DD')
        return redirect('/listadoTratamientos/')



    # Insertando datos mediante ORM de Django
    tratamientoEditar = Tratamiento.objects.get(id=id)
    tratamientoEditar.paciente=pacienteSeleccionado
    tratamientoEditar.tipoTratamiento = tipoTratamiento
    tratamientoEditar.detalles = detalles
    tratamientoEditar.fechaInicio = fechaInicio
    tratamientoEditar.fechaFin = fechaFin
    tratamientoEditar.estado = estado
    tratamientoEditar.instrucciones = instrucciones
    tratamientoEditar.save()

    messages.success(request, 'Editado el tratatamiento exitosamente ')
    return redirect('/listadoTratamientos/')


#33333333333333333333333333333333333333333333333333333333333333333333333

def listadoPago(request):
    pagosBdd =Pago.objects.all()
    pacientesBdd=Paciente.objects.all()
    return render(request, 'pago.html',{'pagos':pagosBdd, 'pacientes':pacientesBdd})

def listadoPagos(request):
    pagosBdd =Pago.objects.all()
    pacientesBdd = Paciente.objects.all()
    return render(request, 'listadoPagos.html', {'pagos':pagosBdd, 'pacientes':pacientesBdd})

def guardarPago(request):
    id_paciente=request.POST["id_paciente"]
    pacienteSelecionado=Paciente.objects.get(id=id_paciente)
    fechaHora = request.POST["fechaHora"]
    totalPago=request.POST["totalPago"].replace(',', '.')  # Reemplaza la coma por punto para números decimales
    montoAbonado=request.POST["montoAbonado"].replace(',', '.')  # Reemplaza la coma por punto para números decimales
    debe=request.POST["debe"].replace(',', '.')  # Reemplaza la coma por punto para números decimales
    estado=request.POST["estado"]



    # Formatear la fecha correctamente



    # Insertando datos mediante ORM de Django
    nuevoPago =Pago.objects.create(
        fechaHora=fechaHora,
        totalPago=totalPago,
        montoAbonado=montoAbonado,
        debe=debe,
        estado=estado,
        paciente=pacienteSelecionado
    )

    messages.success(request, 'Guardado pago exitosamente ')
    return redirect('/listadoPagos/')


def eliminarPago(request, id):
    pagoEliminar = Pago.objects.get(id=id)
    pagoEliminar.delete()
    messages.error(request, 'Eliminado el pago exitosamente ')
    return redirect('/listadoPagos/')


def editarPago(request, id):
    pagoEditar= Pago.objects.get(id=id)
    pacientesBdd = Paciente.objects.all()
    return render(request, 'editarPago.html', {'pago': pagoEditar,'pacientes':pacientesBdd})


def procesarActualizacionPago(request, id):
    #agregamos estas lineas
    id_paciente=request.POST["paciente"]
    pacienteSeleccionado=Paciente.objects.get(id=id_paciente)
    #estas
    fechaHora = request.POST["fechaHora"]
    totalPago=request.POST["totalPago"].replace(',', '.')  # Reemplaza la coma por punto para números decimales
    montoAbonado=request.POST["montoAbonado"].replace(',', '.')  # Reemplaza la coma por punto para números decimales
    debe=request.POST["debe"].replace(',', '.')  # Reemplaza la coma por punto para números decimales
    estado=request.POST["estado"]


    # Formatear la fecha correctamente




    # Insertando datos mediante ORM de Django
    pagoEditar = Pago.objects.get(id=id)
    pagoEditar.paciente=pacienteSeleccionado
    pagoEditar.fechaHora = fechaHora
    pagoEditar.totalPago = totalPago
    pagoEditar.montoAbonado = montoAbonado
    pagoEditar.debe = debe
    pagoEditar.estado = estado
    pagoEditar.save()

    messages.success(request, 'Editado el pago exitosamente ')
    return redirect('/listadoPagos/')


#33333333333333333333333333333333333333333333333333333333333


def listadoClinico(request):
    clinicosBdd = Clinico.objects.all()
    pacientesBdd = Paciente.objects.all()
    citasBdd = Cita.objects.all()
    controlesBdd = Control.objects.all()
    tratamientosBdd = Tratamiento.objects.all()
    return render(request, 'clinico.html', {'clinicos': clinicosBdd, 'pacientes': pacientesBdd, 'citas': citasBdd, 'controles': controlesBdd, 'tratamientos': tratamientosBdd})

def listadoClinicos(request):
    pagosBdd = Pago.objects.all()
    clinicosBdd = Clinico.objects.all()
    pacientesBdd = Paciente.objects.all()
    citasBdd = Cita.objects.all()
    controlesBdd = Control.objects.all()
    tratamientosBdd = Tratamiento.objects.all()
    return render(request, 'listadoClinicos.html', {'pagos': pagosBdd, 'clinicos': clinicosBdd, 'pacientes': pacientesBdd, 'citas': citasBdd, 'controles': controlesBdd, 'tratamientos': tratamientosBdd })

def guardarClinico(request):
    try:
        id_paciente = request.POST["id_paciente"]
        pacienteSeleccionado = Paciente.objects.get(id=id_paciente)
        edadSeleccionado = request.POST["edad"]
        cedulaSeleccionado = request.POST["cedula"]
        fechaSeleccionado = request.POST["fecha"]
        generoSeleccionado = request.POST["genero"]
        descripcionSeleccionado = request.POST["descripcion"]
        fotoSeleccionado = request.POST["foto"]
        tipoTratamientoSeleccionado = request.POST["tipoTratamiento"]
        estadoSeleccionado = request.POST["estado"]

        nuevoClinico = Clinico.objects.create(
            paciente=pacienteSeleccionado,
            edad=edadSeleccionado,
            cedula=cedulaSeleccionado,
            fecha=fechaSeleccionado,
            genero=generoSeleccionado,
            descripcion=descripcionSeleccionado,
            foto=fotoSeleccionado,
            tipoTratamiento=tipoTratamientoSeleccionado,
            estado=estadoSeleccionado
        )

        messages.success(request, 'Guardado el historial clínico exitosamente')
    except Exception as e:
        messages.error(request, f'Error al guardar el historial clínico: {str(e)}')

    return redirect('/listadoClinicos/')

def eliminarClinico(request, id):
    try:
        clinicoEliminar = Clinico.objects.get(id=id)
        clinicoEliminar.delete()
        messages.success(request, 'Eliminado el historial clínico exitosamente')
    except Clinico.DoesNotExist:
        messages.error(request, 'El historial clínico no existe')

    return redirect('/listadoClinicos/')

def editarClinico(request, id):
    try:
        clinicoEditar = Clinico.objects.get(id=id)
    except Clinico.DoesNotExist:
        messages.error(request, 'El historial clínico no existe')
        return redirect('/listadoClinicos/')

    # Aquí puedes agregar el código necesario para obtener los pacientes, citas, controles y tratamientos
    pacientesBdd = Paciente.objects.all()
    citasBdd = Cita.objects.all()
    controlesBdd = Control.objects.all()
    tratamientosBdd = Tratamiento.objects.all()

    # Pasa los datos a la plantilla
    return render(request, 'editarClinico.html', {'clinico': clinicoEditar, 'pacientes': pacientesBdd, 'citas': citasBdd, 'controles': controlesBdd, 'tratamientos': tratamientosBdd})


def procesarActualizacionClinico(request, id):
    try:
        clinicoEditar = Clinico.objects.get(id=id)
        id_paciente = request.POST["id_paciente"]
        pacienteSeleccionado = Paciente.objects.get(id=id_paciente)
        edadSeleccionado = request.POST["edad"]
        cedulaSeleccionado = request.POST["cedula"]
        fechaSeleccionado = datetime.strptime(request.POST["fecha"], "%Y-%m-%d")
        generoSeleccionado = request.POST["genero"]
        descripcionSeleccionado = request.POST["descripcion"]
        # Cambio en la forma de obtener la foto, si está permitido
        tipoTratamientoSeleccionado = request.POST["tipoTratamiento"]
        estadoSeleccionado = request.POST["estado"]

        # Actualizar los atributos del historial clínico
        clinicoEditar.paciente = pacienteSeleccionado
        clinicoEditar.edad = edadSeleccionado
        clinicoEditar.cedula = cedulaSeleccionado
        clinicoEditar.fecha = fechaSeleccionado
        clinicoEditar.genero = generoSeleccionado
        clinicoEditar.descripcion = descripcionSeleccionado
        clinicoEditar.tipoTratamiento = tipoTratamientoSeleccionado
        clinicoEditar.estado = estadoSeleccionado
        clinicoEditar.save()

        messages.success(request, 'Historial clínico editado exitosamente')
    except Paciente.DoesNotExist:
        messages.error(request, 'El paciente seleccionado no existe')
    except ValueError:
        messages.error(request, 'Error al procesar la fecha')
    except Exception as e:
        messages.error(request, f'Error al editar el historial clínico: {str(e)}')

    return redirect('/listadoClinicos/')
